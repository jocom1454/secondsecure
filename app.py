import sqlite3
import string

from flask import Flask, render_template, request
from flask_mail import Mail, Message
import random

app = Flask(__name__)
app.debug = True

app.config['MAIL_SERVER'] = 'smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USE_SSL'] = True

app.config['MAIL_USERNAME'] = 'jigga.blogs@gmail.com'
app.config['MAIL_PASSWORD'] = 'sarahissis'

mail = Mail(app)


@app.route('/')
def hello_world():
    return render_template('index.html')


@app.route('/register')
def registerUser():
    return render_template('register.html')


@app.route('/loggedIn', methods=['POST', 'GET'])
def loggedIn():
    error = None
    if request.method == 'POST':
        # Get string entered by user
        rstring = request.form['rstring']
        # If strings match
        if rstring == temp:
            # Log user in
            return render_template('home.html')
        else:
            error = "wrong verification code"
            return render_template('step2', error)


@app.route('/lastStep')
def lastStep():
    return render_template('step2.html')


@app.route('/addUser', methods=['POST', 'GET'])
def addUser():
    error = None
    if request.method == 'POST':
        # Get information from the form
        username = request.form['username']
        password = request.form['password']
        confirmp = request.form['confirmpassword']

        # check if the passwords are the same, if they are not, do not continue with registration
        if password != confirmp:
            error = "Please ensure your passwords match."
            return render_template('register2.html', error=error)
        # Connect to database
        conn = sqlite3.connect("users.db")
        with conn:
            checkDB = conn.cursor()
            # Check if user already exists
            alreadyExist = checkDB.execute("SELECT * from users where username = ?", (username,)).fetchone()
        # If the user is not already registered
        if alreadyExist is None:
            # Add to database of users
            checkDB.execute(
                "INSERT into users (username, password) VALUES (?, ?)",
                (username, password))
            conn.commit()
            conn.close()  # close the database and open log in page
            return render_template('index.html')
        else:
            error = "this user already exists"
            return render_template('register2.html', error=error)


@app.route('/logIn', methods=['POST', 'GET'])
def logIn():
    error = None
    if request.method == 'POST':
        # Get information from the form
        username = request.form['username']
        password = request.form['password']
        myEmail = username
        # Connect to database
        conn = sqlite3.connect("users.db")
        cursor = conn.cursor()
        # Find user if they exist in the database
        userExists = cursor.execute("SELECT * from users where username = ? AND password = ?",
                                    (username, password)).fetchone()
        # If user does not have an account return error
        if userExists is None:
            error = "this user doesn't exist"
            conn.close()
            return render_template('index.html', error)
        else:
            # If they have an account close the connection to the databsse
            conn.close()
            # Create a random string of length 4
            def randomString(stringLength=4):
                letters = string.ascii_lowercase
                return ''.join(random.choice(letters) for i in range(stringLength))
            # Create global variable
            global temp
            temp = randomString(4)
            print(temp)
            # Create email to send to user
            theCode = temp
            msg = Message(subject='Enter this code to log-in',
                          sender=app.config['MAIL_USERNAME'],
                          recipients=[myEmail])
            msg.body = theCode
            # Send user email and to next page
            mail.send(msg)
            return render_template('loggedIn.html')


if __name__ == '__main__':
    app.run()
